import React, { Component } from 'react';
import './BeatCard.scss';

class BeatCard extends Component {
    render() {
        return (
            <div className="beatCard">
                    
                    <div className="box">
                        <img src={ this.props.beat.image } alt={ this.props.beat.beatName }/>
                    </div>
                    
                    <div>
                        <audio controls>
                            <source src={ this.props.beat.audio } type="audio/mpeg"/>
                            Your browser does not support the audio element.
                        </audio>
                    </div>

                    <h3>
                        <button><a href="/beats/id">{ this.props.beat.beatName }</a></button>
                    </h3>

                    <p>{ this.props.beat.mc } - { this.props.beat.year }</p>
                
                </div>
        )
    }

}

export default BeatCard;
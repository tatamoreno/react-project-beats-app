import React, { Component } from 'react';
import './LyricCard.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

class LyricCard extends Component {
    render() {
        return (
            <div className="card">
                <div className="card__container">
                    <div className="card__container__profile">
                        <div className="pic">
                            <img src={ this.props.lyric.image } alt={ this.props.lyric.songName }/>
                        </div>
                        <div>
                            <p>{ this.props.lyric.mc }</p>
                        </div>
                        <div>
                            <h1>{ this.props.lyric.songName }</h1>
                        </div>
                    </div>

                    <div className="card__container__description">
                        <div>
                            <p>{ this.props.lyric.description }</p>
                        </div>
                    </div>
                </div>
                <div className="card__links">
                    <div>
                        <button><a href="/lyrics/{this.props.lyric.id}">See Details</a></button>
                    </div>
                    <div>
                        <a href="/lyrics/{this.props.lyric.id}/delete">
                        <FontAwesomeIcon className="delete" icon={faTrash} />
                        </a>
                    </div>
                </div>
           </div>
        )
    }

}

export default LyricCard;
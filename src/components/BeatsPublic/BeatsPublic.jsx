import { Component } from 'react';
import BeatCard from '../BeatCard';
import './BeatsPublic.scss';

class BeatsPublic extends Component {
    state = {
        title: 'Listen to your fav beats',
        beatsList: [],
      };
    
      componentDidMount() {
        fetch('http://localhost:4000/beats')
        .then(res => res.json())
        .then(response => this.setState({ beatsList: response }));
      }
    render() {
        return (
            <div className="containerBeat">
                <h2>{this.state.title}</h2>
                <div className="containerBeat__cards">
                {this.state.beatsList.length 
                ?
                (this.state.beatsList.map(beat => {
                    return <BeatCard beat={beat} key={beat._id} />
                    })
                ) 
                : ( <div>There are no files available...</div> ) }
                </div>
            </div>
            )
    }
}

export default BeatsPublic;
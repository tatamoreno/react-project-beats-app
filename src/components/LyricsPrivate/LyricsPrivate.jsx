import { Component } from 'react';
import LyricCard from '../LyricCard';
import './LyricsPrivate.scss';

class LyricsPrivate extends Component {

    state = {
        title: 'Lyrics',
        lyricsList: [],
      };
    
    componentDidMount() {
        fetch('http://localhost:4000/lyrics')
        .then(res => res.json())
        .then(response => {
            console.log('response', response);
            this.setState({ lyricsList: response })
    });
        
    }
    render() {
        return (
            <div className="containerLyric">
                <h2>{this.state.title}</h2>
                <div className="containerLyric__cards">
                    {this.state.lyricsList.length 
                    ?
                    (this.state.lyricsList.map(lyric => {
                        return <LyricCard lyric={lyric} key={lyric._id} />
                        })
                    ) 
                    : ( <div>There are no files available...</div> ) }
                </div>
            </div>
            )
    }
}

export default LyricsPrivate;
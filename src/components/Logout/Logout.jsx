import React, { Component } from 'react';
import './Logout.scss';

class Logout extends Component {

    state = {
        logoTitle: 'Haikus Beats',
      };

    render() {
        return(
            <div className="container">
                <div className="container__box">
                    <img src="/images/logo.png" alt="Logo"/>
                </div>
                <div className="container__title">
                    <h1>{ this.state.logoTitle }</h1>
                </div>
            </div>
        );
    }
}

export default Logo;
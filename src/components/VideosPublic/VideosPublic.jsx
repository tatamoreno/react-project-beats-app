import { Component } from 'react';
import Carousel from '../../containers/Carousel';
import './VideosPublic.scss'

class VideosPublic extends Component {

    state = {
        title: 'Watch your fav videos',
      };

    render() {
        return (<div>
            <h2>{this.state.title}</h2>
            <Carousel/>
        </div>)
    }
}

export default VideosPublic;
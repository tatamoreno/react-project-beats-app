import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faInstagram, faYoutube, faSoundcloud } from '@fortawesome/free-brands-svg-icons';
import './Footer.scss';


class Footer extends Component {
    render() {
        return (
            
            <footer>
                <div className="rounded-social-buttons">
                    <a className="social-button facebook" href="https://www.facebook.com/"><FontAwesomeIcon className="rounded-social-buttons" icon={faFacebook} /></a>
                    <a className="social-button instagram" href="https://www.instagram.com/"><FontAwesomeIcon className="rounded-social-buttons" icon={faInstagram} /></a>
                    <a className="social-button youtube" href="https://www.youtube.com/"><FontAwesomeIcon className="rounded-social-buttons" icon={faYoutube} /></a>
                    <a className="social-button soundcloud" href="https://soundcloud.com/"><FontAwesomeIcon className="rounded-social-buttons" icon={faSoundcloud} /></a>
                </div>
          </footer>
        )
    }

}

export default Footer;
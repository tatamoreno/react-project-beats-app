import React, { Component } from "react";
import { login } from '../../api/auth';
import './LoginForm.scss';

const INITIAL_STATE = {
  email: "",
  password: "",
  error: "",
};

class LoginForm extends Component {
  state = INITIAL_STATE;

  handleSubmitForm = async ev => {
      ev.preventDefault();

      try {
        const data = await login(this.state);
        console.log('LOGIN COMPLETADO', data);
        this.props.saveUser(data);
        this.setState(INITIAL_STATE);
        this.props.history.push('/beats');
      } catch(error) {
        this.setState({ error: error.message });
      }
  }

  handleChangeInput = (ev) => {
    const { name, value } = ev.target;
    this.setState({ [name]: value });
    console.log(name, value);
  };

  render() {
    return (
      <form className="login-form" onSubmit={this.handleSubmitForm}>

        <p className="login-text">
          <span className="fa-stack fa-lg">
            <i className="fa fa-circle fa-stack-2x"></i>
            <i className="fa fa-lock fa-stack-1x"></i>
          </span>
        </p>

        <h1>Login</h1>
        
        <label htmlFor="email">
          <p>Email</p>
          <input
            type="text"
            name="email"
            value={this.state.email}
            className="login-username"
            autoFocus={true}
            placeholder="Email"
            onChange={this.handleChangeInput}
          />
        </label>

        <label htmlFor="password">
          <p>Contraseña</p>
          <input
            type="password"
            name="password"
            value={this.state.password}
            className="login-password"
            placeholder="Password"
            onChange={this.handleChangeInput}
          />
        </label>

        {this.state.error && <p style={{ color: 'white' }}>
          Error en el login: {this.state.error}
        </p>}

        <div style={{ marginTop: '20px' }}>
            <button type="submit" 
            name="Login" 
            value="Login" 
            className="login-submit">Login</button>
        </div>

        <div className="underlay-photo"></div>
        <div className="underlay-black"></div> 
        
      </form>
    );
  }
}

export default LoginForm;

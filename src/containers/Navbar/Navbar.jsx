import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeadphones, faPenAlt, faBook, faHouseDamage, faUserNinja, faVideo  } from '@fortawesome/free-solid-svg-icons';
import Logo from '../../components/Logo';
import './Navbar.scss';
import { Link } from 'react-router-dom';

class Navbar extends Component {

    render() {
        return(
            <nav>
                <Logo/>
                <ul>
                    <li>
                        <Link to="/">
                        <FontAwesomeIcon className="icon" icon={faHouseDamage} />
                        Home</Link>
                    </li>
                    <li>
                        <Link to="/beats">
                        <FontAwesomeIcon className="icon" icon={faHeadphones} />
                        Beats</Link>
                    </li>
                    <li>
                        <Link to="/videos">
                        <FontAwesomeIcon className="icon" icon={faVideo} />
                        Videos</Link>
                    </li>
                    <li>
                        <Link to="/lyrics">
                        <FontAwesomeIcon className="icon" icon={faBook} />
                        Lyrics</Link>
                    </li>
                    <li>
                        <Link to="/freestyle">
                        <FontAwesomeIcon className="icon" icon={faPenAlt} />
                        Freestyle</Link>
                    </li>
                    {!this.props.user &&<li>
                        <Link className="login" to="/login">
                        <FontAwesomeIcon className="icon" icon={faUserNinja} />
                        Login</Link>
                    </li>}

                    {this.props.user && <li>
                        <Link className="login" to="/logout">
                        <FontAwesomeIcon className="icon" icon={faUserNinja} />Logout</Link>
                    </li>}

                </ul>
            </nav>
        );
    }

}

export default Navbar;
import React, { Component } from 'react';
import AwesomeSlider from 'react-awesome-slider';
import withAutoplay from 'react-awesome-slider/dist/autoplay';
import 'react-awesome-slider/dist/styles.css';

const AutoplaySlider = withAutoplay(AwesomeSlider);

class Carousel extends Component {
    state = {
        videoList: [],
      };
    
      componentDidMount() {
        fetch('http://localhost:4000/videos')
        .then(res => res.json())
        .then(response => this.setState({ videoList: response }));
      }

    render(){
        return(
            <div>
                <AutoplaySlider
                    play={true}
                    cancelOnInteraction={false} // should stop playing on user interaction
                    interval={50000}
                    >
                    {this.state.videoList.length 
                    ?
                    (this.state.videoList.map(video => {
                    return <div key={video._id} data-src={video.video} />
                    })
                    ) 
                    : ( <div>There are no files available...</div> ) }
                    {/* <div data-src="https://res.cloudinary.com/dkdvapfsa/video/upload/v1612875731/si_se_puede_y1fdgc.mp4" />
                    <div data-src="https://res.cloudinary.com/dkdvapfsa/video/upload/v1612875688/reflexiones_czmxc8.mp4" />
                    <div data-src="https://res.cloudinary.com/dkdvapfsa/image/upload/v1612270694/kaseo_rk9l2l.jpg" /> */}
                </AutoplaySlider>
            </div>
        )
    }
}

export default Carousel;
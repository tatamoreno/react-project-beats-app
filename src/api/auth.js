const registerUrl = "http://localhost:4000/user/register";
const loginUrl = "http://localhost:4000/user/login";
const checkSessionUrl = "http://localhost:4000/user/check-session";
const logoutUrl = "http://localhost:4000/user/logout";

export const register = async userData => {
  const request = await fetch(registerUrl, {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type":"application/json",
      'Access-Control-Allow-Credentials': true,
      "Access-Control-Allow-Origin": "*",
    },
    credentials: 'include',
    body: JSON.stringify(userData),
  })

  // console.log('request:', request);
  const response = await request.json();

  // console.log('response:', response);

  if(!request.ok) {
    throw new Error(response.message);
  }

  return response;
};

export const login = async userData => {
  const request = await fetch(loginUrl, {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "http://localhost:*",
      "Access-Control-Allow-Credentials": true,
    },
    credentials: 'include',
    body: JSON.stringify(userData),
  })

  const response = await request.json();
  if(!request.ok) {
    return new Error(response.message);
  }

  return response;
}

export const logout = async userData => {
  const request = await fetch(logoutUrl, {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "http://localhost:*",
      "Access-Control-Allow-Credentials": true,
    },
    credentials: 'include',
    body: JSON.stringify(userData),
  })

  const response = await request.json();
  if(!request.ok) {
    return new Error(response.message);
  }

  return response;
}

export const checkSession = async () => {
  const request = await fetch(checkSessionUrl, {
    method: 'GET',
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*"
    },
    credentials: 'include',
  });

  const response = await request.json();
  console.log(response);
  if(!request.ok) {
    throw new Error(response.message);
  }

  return response;
}
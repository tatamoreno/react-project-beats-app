import React, { Component } from 'react';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { checkSession, logout } from './api/auth';

import Navbar from './containers/Navbar';
import Footer from './containers/Footer';
import RegisterForm from './containers/RegisterForm';
import LoginForm from './containers/LoginForm';
import SecureRoute from './components/SecureRoute';
import LyricsPrivate from './components/LyricsPrivate';
import BeatsPublic from './components/BeatsPublic';
import FreestylePrivate from './components/FreestylePrivate';
import VideosPublic from './components/VideosPublic';
import './App.scss';

import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { faHeadphones, faPenAlt, faBook, faPlayCircle, faUserNinja, faVideo  } from '@fortawesome/free-solid-svg-icons';

library.add(fab, faHeadphones, faPenAlt, faBook, faPlayCircle, faUserNinja, faVideo)

class App extends Component {
  state = {
    error: '',
    hasUser: null,
  };

  constructor(props) {
    super(props);
    console.log('Contruyedo el componente - contructor...')
  }

  componentDidMount() {
    this.checkUserSession();
    }

  async checkUserSession() {
    try {
      const data = await checkSession();
      delete data.password;
      this.setState({ user: data, hasUser: true });
    } catch(error) {
      this.setState({ error: error.message, hasUser: false });
    }
  }

  saveUser = user => {
    delete user.password;
    this.setState({ user, hasUser: true });
  };

  // componentWillUnmount() {
  //   console.log('el componente se va a desmontar!')
  // }

  logoutUser = async () => {
    const response = await logout();

    console.log('logout from component', response)
  };


  // state = {
  //     counter: 0,
  //   };

  //   add = ev => {
  //     const newCount = this.state.counter + 1;
  //     this.setState({ counter: newCount });
  //   }

  //   substract = ev => {
  //     const newCount = this.state.counter - 1;
  //     this.setState({ counter: newCount });
  //   }

  render () {
    
    return (
      <Router>
        
        <div className="App">
        <Navbar user ={this.state.user}/>
          <Switch>
          <Route path="/login" component={props => <LoginForm {...props} saveUser={this.saveUser} />}/>
          <Route path="/logout" component={props => <LoginForm {...props} saveUser={this.saveUser} />}/>
          <Route exact path="/" component={props => <RegisterForm {...props} saveUser={this.saveUser} />}/>
          <Route path="/beats" component={props => <BeatsPublic {...props} />} />
          <Route path="/videos" component={props => <VideosPublic {...props} />} />
          <SecureRoute path="/lyrics" hasUser={this.state.hasUser} component={props => <LyricsPrivate {...props} />} />
          <SecureRoute path="/freestyle" hasUser={this.state.hasUser} component={props => <FreestylePrivate {...props} />} />
          </Switch>
          <Footer/>

           {/* <p>Likes: { this.state.counter }</p>
        <button onClick={this.add}>Like</button><button onClick={this.substract}>Dislike</button> */}

        </div>
      </Router>

    );
  }
  }

export default App;
